# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Env Variables

* `LISTEN_PORT` - default `8080`
* `APP_HOST` - default `app`
* `APP_PORT` - default `9000`